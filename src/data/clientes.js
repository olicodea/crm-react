export async function obtenerClientes() {
    const res = await fetch(import.meta.env.VITE_API_URL);
    const result = await res.json();
    return result;
}

export async function obtenerCliente(id) {
    const res = await fetch(`${import.meta.env.VITE_API_URL}/${id}`);
    const result = await res.json();
    return result;
}

export async function agregarCliente(datos) {
    try {
        const resp = await fetch(import.meta.env.VITE_API_URL, {
            method: "POST",
            body: JSON.stringify(datos),
            headers: {
                "Content-type": "application/json",
            },
        });
        await resp.json();
    } catch (error) {
        console.log(error);
    }
}

export async function actualizarCliente(id, datos){
    try {
        const resp = await fetch(`${import.meta.env.VITE_API_URL}/${id}`, {
            method: "PUT",
            body: JSON.stringify(datos),
            headers: {
                "Content-type": "application/json",
            },
        });
        await resp.json();
    } catch (error) {
        console.log(error);
    }
}

export async function eliminarCliente(id){
    try {
        const resp = await fetch(`${import.meta.env.VITE_API_URL}/${id}`, {
            method: "DELETE",
        });
        await resp.json();
    } catch (error) {
        console.log(error);
    }
}